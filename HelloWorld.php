<?php

class HelloWorld {
    private $pdo;

    public function __construct (PDO $pdo) {
        $this -> pdo = $pdo;
    }

    public function hello ($what = 'World') {
        $sql  = "INSERT INTO ";
        $sql .= "hello ";
        $sql .= "VALUES (";
        $sql .= $this->pdo->quote ($what);
        $sql .= ")";

        return "Hello ${what}";
    }

    public function what () {
        $sql = "SELECT what FROM hello";
        $stmt = $this->pdo->query($sql);
        return $stmt->fetchColumn();
    }
}



?>
